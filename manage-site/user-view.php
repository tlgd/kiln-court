<?php

require_once('../config.php');
require_once(BASE_PATH.'/manage-site/manage-site-common.php');

if(($loginUrl = checkLogin()) !== true) { header('Location: '.$loginUrl); exit; }
if($_SESSION['level']<3) { header('Location: /index.php'); exit('Access Denied');  }
$itemType = 'user';

$id = 0;
if(isset($_REQUEST['id']) AND intval($_REQUEST['id']) > 0) $id = intval($_REQUEST['id']); else die('Must supply an ID');

$tableName = 'users';

function changeStatus($newStatus) {
	global $db, $id, $tableName;

	$sql  = 'UPDATE '.mysql_real_escape_string($tableName).' ';
	$sql .= 'SET meta_status = \''.mysql_real_escape_string($newStatus).'\' ';
	$sql .= 'WHERE meta_id = '.intval($id);
	$db->query($sql);
}

if(isset($_REQUEST['submit'])) {
	switch(strtolower($_REQUEST['submit'])) {
		case 'edit': {
			$_REQUEST['ret_url'] = $sections[$itemType]['edit-filename'].'?operation=edit&id='.intval($id);
			$msg = '';
			break;
		}
		case 'enable': {
			changeStatus('live');
			$msg = ucwords($sections[$itemType]['item-name']).' has been set live and can now log in';
			break;
		}
		case 'disable': {
			changeStatus('disabled');
			$msg = ucwords($sections[$itemType]['item-name']).' has been disabled and now cannot log in';
			break;
		}
		case 'delete': {
			deleteDir(_UPLOAD_DIR_.'/user/'.$id);
			changeStatus('deleted');
			$msg = ucwords($sections[$itemType]['item-name']).' has been deleted';
		}
	}
	header('Location: '.((isset($_REQUEST['ret_url']) AND $_REQUEST['ret_url'] != '') ? $_REQUEST['ret_url'].'?' : $sections[$itemType]['index-filename'].'?id='.$id).'&msg='.urlencode($msg));
	exit;
}

$sql = 'SELECT * FROM '.$tableName.' WHERE meta_id = '.intval($id);
$db->query($sql);
if($db->num_rows() != 0) {
	$db->next_record();
	$forename = $db->f('forename');
	$surname = $db->f('surname');
	$email = $db->f('email');
	$office = $db->f('office');
	$status = $db->f('meta_status');
	$level = intval($db->f('level'));
	$company = $db->f('company');
	$password = $db->f('password');
	$username = $db->f('username');
}

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<title>CMS - <?php echo $sections[$itemType]['desc'].' - '.htmlspecialchars($forename.' '.$surname); ?></title>

<link href="css/cms.css" rel="stylesheet" type="text/css" />

</head>

<body>

<div id="header">
	<?php include('./header.php'); ?>
</div>
        
<div class="yellow"></div>

<div id="body_wrapper">			
            
	<h1><?php echo $sections[$itemType]['desc']; ?></h1>
            
            <ul id="breadcrumb">
		<li><a href="index.php">Dashboard</a></li>
		<li>/</li>
		<li><a href="<?php echo $sections[$itemType]['index-filename']; ?>"><?php echo $sections[$itemType]['desc']; ?></a></li>
                <li>/</li>
		<li><?php echo htmlspecialchars($forename.' '.$surname); ?></li>
  </ul>
            
  <div class="clear"></div>
  
	<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="get" enctype="multipart/form-data">

  <div id="button_wrapper">
	  <ul id="buttons">
		  <li class="form_button"><a href="<?php echo $sections[$itemType]['edit-filename']; ?>?id=<?php echo intval($id); ?>&amp;operation=edit">EDIT</a></li>
          <li class="form_button"><a href="<?php echo $sections[$itemType]['view-filename']; ?>?id=<?php echo intval($id); ?>&amp;submit=<?php echo $status == 'live' ? 'disable' : 'enable'; ?>"><?php echo $status == 'live' ? 'DISABLE' : 'ENABLE'; ?></a></li>
		</ul>
        
    <ul id="back_button">
			<li><a href="<?php echo $sections[$itemType]['index-filename']; ?>">Back</a></li>
  		</ul>
  </div>
  
  <div class="clear2"></div>
  
  <div id="intro">
  		<p>Here is where you can find your current profile details. You can edit your name and password details simply by clicking <strong>EDIT</strong> above.</p>
  </div>
  
  <div class="clear2"></div>
  
  <div class="profilebox"> <!-- was "article_date" -->
  
	  <h5>Email</h5>

      <div class="copy_in">
        <p><?php echo htmlspecialchars($email); ?></p>
      </div>
      
    <div id="info2"><img src="images/icons/info.png" id='article_date' width="19" height="19" /></div>
  
  </div>
  
  <div class="profilebox_alt"> <!-- was "article_date" -->
  
	  <h5>Login</h5>

      <div class="copy_in">
        <p><?php echo htmlspecialchars($username); ?></p>
      </div>
      
    <div id="info2"><img src="images/icons/info.png" id='article_date' width="19" height="19" /></div>
  
  </div>
  
  <div class="profilebox"> <!-- was "article_date" -->
  
	  <h5>Forename</h5>

      <div class="copy_in">
        <p><?php echo htmlspecialchars($forename); ?></p>
      </div>
      
      <div id="info2"><img src="images/icons/info.png" id='article_date' width="19" height="19" /></div>
  
  </div>
  
  <div class="profilebox_alt"> <!-- was "article_date" -->
  
	  <h5>Surname</h5>

      <div class="copy_in">
        <p><?php echo htmlspecialchars($surname); ?></p>
      </div>
      
      <div id="info2"><img src="images/icons/info.png" id='article_date' width="19" height="19" /></div>
  
  </div>
  
  <div class="profilebox_alt"> <!-- was "article_date" -->
  
	  <h5>Level</h5>

      <div class="copy_in">
        <p><?php echo htmlspecialchars($level); ?></p>
      </div>
      
      <div id="info2"><img src="images/icons/info.png" id='article_date' width="19" height="19" /></div>
  
  </div>
  &nbsp;
  <div class="profilebox_alt"> <!-- was "article_date" -->
  
	  <h5>Company</h5>

      <div class="copy_in">
        <p><?php echo htmlspecialchars($company); ?></p>
      </div>
      
      <div id="info2"><img src="images/icons/info.png" id='article_date' width="19" height="19" /></div>
  
  </div>
  <div class="profilebox_alt"> <!-- was "article_date" -->
  
	  <h5>Password</h5>

      <div class="copy_in">
        <p><?php echo htmlspecialchars($password); ?></p>
      </div>
      
      <div id="info2"><img src="images/icons/info.png" id='article_date' width="19" height="19" /></div>
  
  </div>

<?php /*
  <div class="profilebox"> <!-- was "article_date" -->
  
	  <h5>Password</h5>

      <div class="copy_in">
        <p>************</p>
      </div>
      
      <div id="info2"><img src="images/icons/info.png" id='article_date' width="19" height="19" /></div>
  
  </div>
  
  <div class="profilebox_alt"> <!-- was "article_date" -->
  
	  <h5>Re-enter Password</h5>

      <div class="copy_in">
        <p>************</p>
      </div>
      
      <div id="info2"><img src="images/icons/info.png" id='article_date' width="19" height="19" /></div>
  
  </div>
*/ ?>

  </div>
<div class="clear"></div>

<div class="yellow2"></div>
<div id="footer">
	<ul id="help">
		
	</ul>
</div>

</body>
</html>
