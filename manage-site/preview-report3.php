<?php
require_once('../config.php');
require_once(BASE_PATH.'/manage-site/manage-site-common.php');
require_once(BASE_PATH.'/includes/class.dblister.php');

if(($loginUrl = checkLogin()) !== true) { header('Location: '.$loginUrl); exit; }
if($_SESSION['level']<3) { header('Location: /index.php'); exit('Access Denied');  }
if (isset($_POST['from']) && $_POST['from']<>"") $from = $_POST['from'];
	else $from = 1;
	
if (isset($_POST['to'])) $to = $_POST['to'];
	else $to = $from+9;
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Agents Insight - Dataroom</title>


<link href="css/dataroom-edit.css" rel="stylesheet" type="text/css" />

<link type="text/css" href="css/custom-theme/jquery-ui-1.8.16.custom.css" rel="stylesheet" />	
		<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script>
		<script type="text/javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
        
        <!-- add for multiselect -->
        <script type="text/javascript" src="js/jquery.multiselect.js"></script>

<script type="text/javascript">
$(function() {
		
	// Datepicker
				$('.dates').datepicker({
					showOn: "button",
			buttonImage: "images/icons/calendar_icon.png",
			buttonImageOnly: true
				});
		
	});
</script>

<script type="text/javascript">

$(function(){

	$("select.multi").multiselect();

});

</script>

<script type="text/javascript">
function MM_jumpMenu(targ,selObj,restore){ //v3.0
  document.forms['specform'].elements['from'].value = 1;
  document.forms['specform'].submit();
}
function MM_jumpMenu2(targ,selObj,restore){ //v3.0
	document.forms['specform'].elements['from'].value = <?php echo (intval($from-$to)<=0 ? '1' : intval($from-$to)) ?>;
	document.forms['specform'].submit();
}
function MM_jumpMenu3(targ,selObj,restore){ //v3.0
	document.forms['specform'].elements['from'].value = <?php echo $from+$to ?>;
	document.forms['specform'].submit();
}
</script>

</head>

<body id="addCompany">
<div id="header">
<div id="header_wrapper">

                <div id="logo"><img src="images/logo.png"></div>

                <ul id="name">

					
                    <li class="white" style="width:80px;">Michael Avery</li>

                 
                                <li class="downarrow_1">Admin</li>
                                
                               <li class="white">|</li>
					   <li class="settings_1">Settings</li>
                               

    </ul>

				 <ul class="the_menu_1 the_menu">
                                    <span class="white1"><strong>Settings</strong></span>
                                    <li><a href="specification-listing.php">Edit Specifications</a></li>
                                    

    </ul>
<ul class="the_menu_2 the_menu">
                                    <span class="white1"><strong>Admin Links</strong></span>
                                    <li><a href="http://tlgd.zendesk.com" target="_blank">Support Website</a></li>
                                    <li><a href="mailto:support@tlgd.zendesk.com">Contact Support</a></li>
                                    <li><a href="/index.php?logout=1">Log out</a></li>

    </ul>




	


                
  </div>
</div>

<div id="body_wrapper">

<h1>Dataroom</h1>

 <ul id="breadcrumb">
    <li><a href="#">Dataroom /</a></li>
     <li><a href="#">Reports /</a></li>
     <li><a href="#" class="active">Preview Report</a></li>	            
    </ul>
    
       <div class="yellowunBound1">
            <div class="floatRight">
            <a href="download.php?file=report3.csv" class="active">Download</a>
       		<a href="user-listing.php">Cancel</a>
       </div>
           </div>
          

<div id="formContainer">

<div id="panel_nav_container">

		<div id="navigation">
        		<ul class="yellowStrip">
                    <li><a href="user-listing.php">Users</a></li>
                    <li><a href="preview-report1.php">Reports</a></li>
                    <ul>
                    	  <li><a href="preview-report1.php">User Overview</a></li>
                    	  <li><a href="preview-report2.php">User Detail</a></li>
                    	  <li><a href="preview-report3.php" class="active">Pages Viewed</a></li>
                    	  <li><a href="preview-report4.php">Downloads Overview</a></li>
                  	  	</ul>
                </ul>
        </div>
</div>
 
   <div id="adminSideRight" class="marginb">        
   
   <div id="downarrow_grey_one"><img src="images/maindown_arrow_1.gif" width="25" height="10" /></div>
   <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" id="specform" name="specform"> 
   	<h2>Your Report</h2>
    
    <dl class="addDetails">
      <div class="clear"></div>
    
    <table width="350" border="0" cellspacing="0" cellpadding="0" class="reportsummary">
  <tr>
    <th scope="row" width="80px">User(s):</th>
    	<td>
    	<?php
    	$assignedusers = array();
    	if (isset($_REQUEST['users']))
    		$assignedusers = $_REQUEST['users'];
    	echo outputSelect('users', getUsers(true), $assignedusers, false, false, 'multi', false, null, false, true); 
    	?>
    	</td>
  </tr>
  <tr>
    <th scope="row" width="80px">Date from:</th>
    <td><input type="text" name="datefrom" class="dates" value="<?php if (isset($_REQUEST['datefrom'])) echo $_REQUEST['datefrom']; ?>"></td>
  </tr>
  <tr>
    <th scope="row" width="80px">Date to:</th>
    <td><input type="text" name="dateto" class="dates" value="<?php if (isset($_REQUEST['dateto'])) echo $_REQUEST['dateto']; ?>"></td>
  </tr>
 
</table>
    <input type="hidden" name="submit_confirm" id="submit_confirm" value="1" />
    <input class="whiteButton margTop" type="submit" name="submit_next" id="submit_next" value="Update Report" />
  	<!--<a id="edit-report-btn" href="#" title="Edit"><span>Edit</span></a>
  	<a id="download-report-btn" href="#" title="Download"><span>Download</span></a>
    <a href="report.csv" target="_blank" style="color:#ffcc00;">Download Report</a>-->	
    
    </dl>
    
   </div>
   
   <div id="listcontainer">
   
   <table width="720" border="0" cellspacing="0" cellpadding="0" id="reportstable">
  <tr>
    <th colspan="2" scope="col">Report Preview</th>
    </tr>
  <tr>
    <th scope="col" class="main" >Page Name</th>
    <th scope="col" class="main" >Times Viewed</th>
  </tr>
  <?php 
	if(isset($_REQUEST['submit_confirm'])) {
		$displaydateTimestamp1 = ukDateToTimestamp($_REQUEST['datefrom']);
		$displaydateTimestamp2 = ukDateToTimestamp($_REQUEST['dateto'].' 23:59:59');
		  //echo $_REQUEST['datefrom'].'<br>';
		  //echo $_REQUEST['dateto'].'<br>';
		  //echo "<pre>";
		  //print_r($_REQUEST['users']);
		  //echo "</pre>";
		  if (isset($_REQUEST['users'])){
			  $fp = fopen(BASE_PATH."/manage-site/report3.csv", "w");
			  $list = array ('Page', 'View count');
			  fputcsv($fp, $list);
			  $users = '';
			  $i = 0;
			  foreach ($_REQUEST['users'] as $key => $value){
			  	  $i++;
			  	  if ($i == 1) $users .= $value;
			  	  else $users .= ', '.$value;
			  }
					//$sql = 'SELECT * FROM log_download d, users u WHERE d.user_id = '.$value.' AND d.date >= FROM_UNIXTIME('.$displaydateTimestamp1.') AND d.date <= FROM_UNIXTIME('.$displaydateTimestamp2.') AND d.user_id = u.meta_id';
					$sql = 'select 
							count(user_id) as viewed,
							page
						from 
							log_pages
						where
							log_pages.date >= FROM_UNIXTIME('.$displaydateTimestamp1.') AND log_pages.date < FROM_UNIXTIME('.$displaydateTimestamp2.') AND user_id in ('.$users.')
						group by
							page
						order by
							viewed desc';
					//echo $sql.'<br>';
					$db->query($sql);
					$i=0;
					$ip = 1;
					$total=0;
					while ($db->next_record()){
						$i++;
						$list = array ($db->f('page'), $db->f('viewed'));
						fputcsv($fp, $list);
						if ($ip >= $from AND $ip <$from+$to) {
						?>
						<tr <?php if ($i==2) {echo 'class="greyone"'; $i=0; } ?> >
						    <td><?php echo $db->f('page');?></td>
						    <td><?php echo $db->f('viewed');?></td>
						  </tr>
					<?php
						}
					$ip++;
					$total++;
					}
			  
			  fclose($fp);
		  }
  }
?>
  
</table>

<div class="clear"></div>


<div id="bottom_sort">
        
        <p>Show rows:</p> 
        <!--<form name="form" id="sort_rows" action="" method="get">-->
          <select name="to" id="to" onchange="MM_jumpMenu('parent',this,0)" style="margin: 10px">
            <option value="10" <?php if ($to == 10) echo "selected" ?>>10</option>
            <option value="20" <?php if ($to == 20) echo "selected" ?>>20</option>
            <option value="30" <?php if ($to == 30) echo "selected" ?>>30</option>
          </select>
            
        <div id="page_sort">
        
       <?php if ($i > 0) 
        {
        	echo "<p>Showing ";
        
        	$end = ($ip>=$to) ? $from+$to-1 : $from+$ip-2; echo $from.' - '.$end;
        	
        	echo " of ".$total." results</p>";
        }
        else
        	echo "<p>No results</p>";
	if($end==1 || !$end) $end = 10;
        ?>
 
        <input type="hidden" name="from" id="from" value="<?=intval($from)?>">
        
        <input type="button" id="next" title="Next" onclick="MM_jumpMenu3('parent',this,0)" <?php if($total<=$end) echo 'disabled="disabled"'; ?> >
        <input type="button" id="prev" title="Previous" onclick="MM_jumpMenu2('parent',this,0)">
        
        <!--<a id="prev" href="<? echo $_SERVER['PHP_SELF'].'?to='.intval($to).'&from='.(intval($from-$to)<=0 ? '1' : intval($from-$to)); ?>" title="Previous" onclick="MM_jumpMenu2('parent',this,0)"><span>Previous</span></a>-->         

        </div>
        
      </div>

   </div>
   <div class="clear"></div>
</div>
</form>

           
       <div class="yellowunBound1">
            <div class="floatRight">
            <a href="download.php?file=report3.csv" class="active">Download</a>
       		<a href="user-listing.php">Cancel</a>
       </div>
       </div>




</div>

<div id="footer">
<p><a href="#">Help ?</a></p>
</div>

</body>
</html>
