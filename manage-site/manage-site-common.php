<?php  if ( ! defined('BASE_PATH')) exit('No direct script access allowed');

//session_start();
//require_once('../config.php');
require_once(BASE_PATH.'/includes/common.php');

$sections = array();
// array index = filename stem and database name
/*
								'edit-filename' => 'property-edit.php');
*/
$sections['property'] = array(	'css-name' => 'property',
								'desc' => 'Properties',
								'item-name' => 'property',
								'index-filename' => 'admin-menu-index.php',
								'view-filename' => 'Summarymain-index.php',
								'edit-filename' => 'Summarymain-index.php',
								'edit-filename1' => 'properties/property.php');
/*
$sections['property'] = array(	'css-name' => 'property',
								'desc' => 'Properties',
								'item-name' => 'property',
								'index-filename' => 'property-listing.php',
								'view-filename' => 'property-view.php',
*/

$sections['publication'] = array(	'css-name' => 'publication',
									'desc' => 'Publications',
									'item-name' => 'publication',
									'index-filename' => 'publication-listing.php',
									'view-filename' => 'publication-view.php',
									'edit-filename' => 'publication-edit.php');
$sections['link'] = array(	'css-name' => 'link',
							'desc' => 'Links',
							'item-name' => 'link',
							'index-filename' => 'link-listing.php',
							'view-filename' => 'link-view.php',
							'edit-filename' => 'link-edit.php');
$sections['news'] = array(	'css-name' => 'news',
							'desc' => 'News',
							'item-name' => 'news',
							'index-filename' => 'news-listing.php',
							'view-filename' => 'news-view.php',
							'edit-filename' => 'news-edit.php',
							'edit-filename2' => 'news-edit2.php');
$sections['report'] = array(	'css-name' => 'report',
								'desc' => 'Reports',
								'item-name' => 'report',
								'index-filename' => 'report-listing.php',
								'view-filename' => 'report-view.php',
								'edit-filename' => 'report-edit.php');
$sections['contact'] = array(	'css-name' => 'contact',
								'desc' => 'Contacts',
								'item-name' => 'contact',
								'index-filename' => 'contact-listing.php',
								'view-filename' => 'contact-view.php',
								'edit-filename' => 'contact-edit.php');
$sections['user'] = array(	'css-name' => 'users',
							'desc' => 'Users',
							'item-name' => 'user',
							'index-filename' => 'user-listing.php',
							'view-filename' => 'user-view.php',
							'edit-filename' => 'user-edit.php');
$sections['user-profile'] = array(	'css-name' => 'users',
									'desc' => 'My Profile',
									'item-name' => 'My profile',
									'index-filename' => 'profile-view.php',
									'view-filename' => 'profile-view.php',
									'edit-filename' => 'profile-edit.php');
$sections['specification'] = array(	'css-name' => 'specifications',
									'desc' => 'Specifications',
									'item-name' => 'specification',
									'index-filename' => 'specification-listing.php',
									'view-filename' => 'specification-view.php',
									'edit-filename' => 'specification-edit.php');

$sections['prefix'] = array(	'css-name' => 'prefixes',
									'desc' => 'Prefixes',
									'item-name' => 'prefix',
									'index-filename' => 'prefix-listing.php',
									'view-filename' => 'prefix-view.php',
									'edit-filename' => 'prefix-edit.php');



$userTypes = array('user' => 'Standard CMS user', 'admin' => 'Admin user - can manage CMS users');

function deleteDir($dir) {
	if(!is_dir($dir)) return true;

	// delete all the files in the dir
	$files = glob($dir.'/*');
	foreach($files AS $entry)
		if($entry != '.' AND $entry != '..')
			if(is_dir($entry))
				deleteDir($entry);
			else
				unlink($entry);

	// now delete the dir itself and return the success of this command
	return rmdir($dir);
}

// $localFile - allows us to use rename() instead of the (more secure of genuinely-uploaded files) move_uploaded_file()
function saveFile($id, $fileType, $i, $section, $localFile = 0, $sourceFileName, $name) {
	global $imageSizes, $imagePanelSizes;

	if($section == 'imagepanel') $imageSizes = $imagePanelSizes;

	// firstly delete any existing files
	$dirToDelete = _UPLOAD_DIR_.'/'.$section.'/'.$id.'/'.$fileType.'s/'.$i;
	if(!deleteDir($dirToDelete)) return false;

	$destDir = _UPLOAD_DIR_.'/'.$section.'/'.$id;
	if(!is_dir($destDir))
		if(!mkdir($destDir)) return false;

	$destDir = $destDir.'/'.$fileType.'s';
	if(!is_dir($destDir))
		if(!mkdir($destDir)) return false;

	$destDir .= '/'.$i;
	if(!is_dir($destDir))
		if(!mkdir($destDir)) return false;

	$destFileName = $destDir.'/'.makeSafeFilename($name);

	if($localFile==2)
        {
            //echo "source : $sourceFileName and destination : $destFileName";
            copy($sourceFileName, $destFileName); 
        }
        else if($localFile) {
		if(!rename($sourceFileName, $destFileName)) // $filesRef was $fileType.$i
			return false;
	}
	else
		if(!move_uploaded_file($sourceFileName, $destFileName)) // $filesRef was $fileType.$i
			return false;

	if($fileType == 'image')
		foreach($imageSizes AS $imageFileName => $imageDetails)
			resizeImage($destFileName, $destDir.'/'.$imageFileName, $imageDetails['width'], $imageDetails['height']);

	return true;
}

function makeSafeFilename($filename) {
	return preg_replace('|[^A-Z0-9\-\.]|i', '', str_replace('..', '', $filename));
}

function showMessages($messages) {
	if(is_array($messages) AND count($messages) > 0)
		echo '<div class="messages"><ul><li>'.implode('</li><li>', $messages).'</li></ul></div>';
}

function resizeImage($srcImage, $dstImage, $dstWidth, $dstHeight) {
	$size = getimagesize($srcImage); // get the width and height of the original image

	$srcWidth = $size[0];
	$srcHeight = $size[1];

	$resizedWidth = $srcWidth;
	$resizedHeight = $srcHeight;
	if($srcHeight > $srcWidth) {
		if($resizedWidth > $dstWidth) {
			$aspectRatio = $dstWidth / $resizedWidth;
			$resizedWidth = round($aspectRatio * $resizedWidth);
			$resizedHeight = round($aspectRatio * $resizedHeight);
		}
	}
	else {
		if($resizedHeight > $dstHeight) {
			$aspectRatio = $dstHeight / $resizedHeight;
			$resizedWidth = round($aspectRatio * $resizedWidth);
			$resizedHeight = round($aspectRatio * $resizedHeight);
		}
	}

	$theImage = imagecreatetruecolor($dstWidth, $dstHeight);

	$background = imagecolorallocate($theImage, 255, 255, 255);
	imagefilledrectangle($theImage, 0, 0, $dstWidth, $dstHeight, $background);

	$imageSource = imagecreatefromjpeg($srcImage);

	$dstX = round(($dstWidth - $resizedWidth) / 2);
	$dstY = round(($dstHeight - $resizedHeight) / 2);
	$result = imagecopyresampled($theImage, $imageSource, $dstX, $dstY, 0, 0, $resizedWidth, $resizedHeight, $srcWidth, $srcHeight);

	$result = imagejpeg($theImage, $dstImage, 100);

	unset($theImage); unset($background); unset($imageSource); unset($result); // some clean-ups to free some memory
}

// returns a boolean indicating whether an e-mail address is valid or now
function isValidEmail($email) {
	if(strlen($email) < 5) return false;
	elseif(!preg_match("/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z]+$/", $email)) return false;
	elseif($_SERVER['SERVER_ADDR'] != '192.168.56.101') {
		list($username, $domain) = split('@', $email);
		$mxhosts = array();
		//if(!getmxrr($domain, $mxhosts)) return false;
	}
	return true;
}

// returns true if they're logged in (so check for "checkLogin() === true"), otherwise the log in URL is returned
function checkLogin($requiredLevels = array()) {
	if(isset($_SESSION['login_id']) AND intval($_SESSION['login_id']) > 0) {
		if(count($requiredLevels) == 0)
			return true;
		elseif(in_array($_SESSION['login_type'], $requiredLevels))
			return true;
	}
	return SITE_URL.'/index.php?ret_url='.urlencode(strip_tags($_SERVER['REQUEST_URI']));
}

// re-format the 'name' attribute in $guideAuthors to a single dimensional array which can be passed to outputSelect
function getGuideAuthorNames() {
	global $guideAuthors;

	$guideAuthorsNames = array();
	foreach($guideAuthors as $ref => $details)
		$guideAuthorsNames[$ref] = $details['name'];
	return $guideAuthorsNames;
}

function loadOffices() {
	global $db;

	$offices = array();

	$sql = 'SELECT * FROM offices WHERE meta_status = \'published\'';
	$db->query($sql);
	while($db->next_record())
		$offices[$db->f('meta_id')] = $db->f('title');

	return $offices;
}

?>