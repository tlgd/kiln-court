<?php 
/**
 *
 * Configuration file
 */
error_reporting(0);
ini_set('display_errors', 0);
/**
 * Database configuration
 */

/* Remote config */
define('DB_HOST','localhost');
define('DB_NAME','kiln_court');
define('DB_USER','kiln_court');
define('DB_PASSWORD','6f43rfdsf234rf');
define('SITE_URL','http://www.kilncourt.co.uk');

/* Local config 
define('DB_HOST','localhost:8889');
define('DB_NAME','kilncourt');
define('DB_USER','root');
define('DB_PASSWORD','root');
define('SITE_URL','http://localhost:8888/kilncourt'); */

/**
 *  Base path of the application
 */
define('BASE_PATH',dirname(__FILE__));

$title = 'Kiln Court- Martine Waghorn Data Rooms';
$shortTitle = 'Kiln Court';
$client = 'Martine Waghorn';
$mailto = 'rmm@martinewaghorn.co.uk';

?>