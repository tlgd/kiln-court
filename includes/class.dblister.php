<?php

class dbLister
{
	var $db;

	var $filters = array(); // left-hand-side filters

	var $rows = array(); // will hold the data
	var $sqlQuery = ''; // will hold the query

	var $extraColumns = array(); // any columns in addition to "*"

	var $whereCondition = '';
	var $havingCondition = '';

	// listing defaults
	//var $listingOrder = 'asc';
	//var $orderCol = '';
	var $currentPage = 0;
	var $quantityPerPage = 0;
	var $paging = 'paged';	// or 'viewall'

	var $orderBys = array();
	var $groupBys = array();

	// populated by the object
	var $numberOfResults;
	var $startRow = 0;
	
	//joining for table 
	var $sqlJoin ='';

	var $queryStringArgs = array();

	// ------------------------------------------------------------------------------------------------------------------------------
	// ------------------------------------------------------------------------------------------------------------------------------
	function dbLister($table, $fieldNameStem, $defaultOrderCol, $quantityPerPage = 3, $orderDir = 'asc', $allowedOrderByCols = array()) {
		$this->table = $table;
		$this->fieldNameStem = $fieldNameStem;
		//$this->orderCol = $defaultOrderCol;
		if(is_array($defaultOrderCol)) $this->orderBys = $defaultOrderCol;
		else $this->orderBys[$defaultOrderCol] = $orderDir;
		$this->quantityPerPage = $quantityPerPage;
		$this->db = new some_db;

		// only columns specified in this array can be be used for sorting results. This is because the column name is passed through the $_REQUEST so needs to be checked before going into the SQL
		// format: array(<display_name> => <field_name>)
		$this->allowedOrderByCols = $allowedOrderByCols;

		if(isset($_REQUEST['ordercol']) AND array_key_exists($_REQUEST['ordercol'], $this->allowedOrderByCols))
			$this->orderBys = array(mysql_real_escape_string($this->allowedOrderByCols[$_REQUEST['ordercol']]) => (isset($_REQUEST['order']) AND ($_REQUEST['order'] == 'asc' OR $_REQUEST['order'] == 'desc')) ? $_REQUEST['order'] : $orderDir) + $this->orderBys;

		$cookieNamePrefix = preg_replace('|[^A-Z0-9]|i', '', $_SERVER['PHP_SELF']).'_';

		// firstly, check for any cookie set data
		if(isset($_REQUEST['paging'])) {
			if($_REQUEST['paging'] == 'viewall') {
				$this->paging = 'viewall';
				$this->quantityPerPage = 0;
			}
			else
				$this->paging = 'paged';
			@setcookie($cookieNamePrefix.'c_pagingprefs', $this->paging, time()+60*60*24*30, '/');
		}
		elseif(isset($_COOKIE[$cookieNamePrefix.'c_pagingprefs']) AND $_COOKIE[$cookieNamePrefix.'c_pagingprefs'] == 'viewall') {
			$this->paging = 'viewall';
			$this->quantityPerPage = 0;
		}

		if(isset($_REQUEST['page']) AND intval($_REQUEST['page']) != 0)
			$this->currentPage = intval($_REQUEST['page']);

		if($this->paging == 'viewall') $this->quantityPerPage = 0;
	}

	// ------------------------------------------------------------------------------------------------------------------------------
	// ------------------------------------------------------------------------------------------------------------------------------
	function addExtraColumn($columnAlias, $sql) {
		$this->extraColumns[$columnAlias] = $sql;
	}

	// ------------------------------------------------------------------------------------------------------------------------------
	// ------------------------------------------------------------------------------------------------------------------------------
	function addGroupBy($column) {
		$this->groupBys[] = $column;
	}

	// ------------------------------------------------------------------------------------------------------------------------------
	// ------------------------------------------------------------------------------------------------------------------------------
	// takes an array in the format (order_col_field_name => direction)
	function processAdditionalOrderBys($additionalOrderBys) {
		$newOrderBys = array();
		foreach($additionalOrderBys AS $col => $dir)
			if(!isset($this->orderBys[$col]) AND array_key_exists($col, $this->allowedOrderByCols))
				//$this->orderBys[$col] = $dir;
				$newOrderBys[$col] = $dir;
		if(count($newOrderBys) > 0) $this->orderBys = $newOrderBys + $this->orderBys;
	}

	// ------------------------------------------------------------------------------------------------------------------------------
	// ------------------------------------------------------------------------------------------------------------------------------
	// get a list of products which match our criteria
	function addFilters($filters) {
		foreach($filters AS $key => $val)
			$this->filters[$key] = $val;
	}

	// ------------------------------------------------------------------------------------------------------------------------------
	// ------------------------------------------------------------------------------------------------------------------------------
	// get number of rows which match our criteria
	function setNumberOfResults() {
		$this->numberOfResults = 0;

		// if there is a HAVING condition then have to do the look-up a slightly different way (can't use COUNT(*) because it gives a "Mixing of GROUP columns (MIN(),MAX(),COUNT(),...) with no GROUP columns is illegal if there is no GROUP BY clause" error
		if($this->havingCondition != '') {
			$sql  = 'SELECT * ';
			if(count($this->extraColumns) > 0)
				foreach($this->extraColumns AS $columnAlias => $columnSql)
					$sql .= ', '.$columnSql.' AS '.$columnAlias.' ';
			$sql .= 'FROM '.mysql_real_escape_string($this->table).' ';
			//added by Roshan for join
			if($this->sqlJoin != '') $sql .= ' '.$this->sqlJoin.' ';
			if($this->whereCondition != '') $sql .= 'WHERE '.$this->whereCondition.' ';
			$sql .= 'HAVING '.$this->havingCondition.' ';
			$this->db->query($sql);
			$this->numberOfResults = $this->db->num_rows();
		}
		else {
			$sql  = 'SELECT COUNT(*) AS number_of_matches ';
			
			$sql .= 'FROM '.mysql_real_escape_string($this->table).' ';
			if($this->sqlJoin != '') $sql .= ' '.$this->sqlJoin.' ';
			if($this->whereCondition != '') $sql .= 'WHERE '.$this->whereCondition.' ';
			//echo $sql; 
			$this->db->query($sql);
			if($this->db->num_rows() == 1) {
				$this->db->next_record();
				$this->numberOfResults = intval($this->db->f('number_of_matches'));
			}
		}
	}

	// ------------------------------------------------------------------------------------------------------------------------------
	// ------------------------------------------------------------------------------------------------------------------------------
	// get a list of rows which match our criteria
	function getMatches($outputSql = false) {
		$sql  = 'SELECT * ';
		if(count($this->extraColumns) > 0)
			foreach($this->extraColumns AS $columnAlias => $columnSql)
				$sql .= ', '.$columnSql.' AS '.$columnAlias.' ';
		$sql .= 'FROM '.mysql_real_escape_string($this->table).' ';
		//added by ROshan for join condition 
		if($this->sqlJoin != '') $sql .= ' '.$this->sqlJoin.' ';
		if($this->whereCondition != '') $sql .= 'WHERE '.$this->whereCondition.' ';
		if($this->havingCondition != '') $sql .= 'HAVING '.$this->havingCondition.' ';
		if(count($this->groupBys) > 0) $sql .= 'GROUP BY '.implode(', ', $this->groupBys).' ';

		$sql .= $this->getOrderByClause();

		if($this->paging != 'viewall' AND $this->quantityPerPage > 0) // make sure we only pull in the rows we require
			$sql .= 'LIMIT '.intval($this->startRow).', '.intval($this->quantityPerPage);

		$this->sqlQuery = $sql;
if($outputSql) echo $this->sqlQuery;
		$this->db->query($sql);
		if($this->db->num_rows() != 0) {
			$this->rowsToDisplay = array();
			while($this->db->next_record())
				$this->rowsToDisplay[] = $this->db->Record;
		}
	}

	// ------------------------------------------------------------------------------------------------------------------------------
	// ------------------------------------------------------------------------------------------------------------------------------
	function getOrderByClause() {
		//if($this->orderCol AND $this->listingOrder)
		//	return 'ORDER BY '.mysql_real_escape_string($this->orderCol).' '.strtoupper($this->listingOrder).' ';
		$orderBySqlParts = array();
		foreach($this->orderBys AS $col => $direction)
			$orderBySqlParts[] = mysql_real_escape_string($col).' '.($direction == 'desc' ? 'DESC' : 'ASC');
		if(count($orderBySqlParts) > 0)
			return ' ORDER BY '.implode(', ', $orderBySqlParts).' ';
	}

	// ------------------------------------------------------------------------------------------------------------------------------
	// ------------------------------------------------------------------------------------------------------------------------------
	function populateQueryStringArgs($filtersToExclude = array()) {
		//$this->queryStringArgs = array();

		$workingQueryString = '';
		foreach($this->filters AS $item => $value)
			if(!in_array($item, $filtersToExclude))
				$this->queryStringArgs[$item] = $value;

		// get primary order by details
		reset($this->orderBys);
		list($this->queryStringArgs['ordercol'], $this->queryStringArgs['order']) = each($this->orderBys);
	}

	// ------------------------------------------------------------------------------------------------------------------------------
	// ------------------------------------------------------------------------------------------------------------------------------
	// $columnToSortOn must be the user-presentable version, not the actual SQL column name
	function getSortLink($columnToSortOn = null, $toggleSort = true, $filtersToExclude = array(), $validHtml = true) {
		$this->populateQueryStringArgs($filtersToExclude);

		if($columnToSortOn == null) {
			// get primary order by details
			reset($this->orderBys);
			list($currentOrderCol, $currentOrderDir) = each($this->orderBys);

			$columnToSortOn = array_search($currentOrderCol, $this->allowedOrderByCols);
		}

		// use a local copy of $this->queryStringArgs because we don't want to actually update the live version should we want to use it elsewhere
		$queryStringArgs = $this->queryStringArgs;

		if($toggleSort === true) {
			$queryStringArgs['ordercol'] = $columnToSortOn;

			if($columnToSortOn == $currentOrderCol) {
				if($currentOrderDir == 'asc')
					$queryStringArgs['order'] = 'desc';
				else
					$queryStringArgs['order'] = 'asc';
			}
			else
				$queryStringArgs['order'] = 'asc';
		}
		elseif($toggleSort == 'asc' OR $toggleSort == 'desc') {
			$queryStringArgs['ordercol'] = $columnToSortOn;
			$queryStringArgs['order'] = $toggleSort;
		}
		else {
			$queryStringArgs['ordercol'] = $columnToSortOn;
			$queryStringArgs['order'] = $currentOrderDir;
		}

		$workingQueryString = '';
		foreach($queryStringArgs AS $arg => $val)
			if(!in_array($arg, $filtersToExclude))
				if(gettype($val) == 'array')
					foreach($val AS $value)
						$workingQueryString .= ($validHtml == true ? '&amp;' : '&').urlencode($arg).($validHtml == true ? '%5B%5D' : '[]').'='.urlencode($value);
				else
					$workingQueryString .= ($validHtml == true ? '&amp;' : '&').urlencode($arg).'='.urlencode($val);

		return $_SERVER['PHP_SELF'].'?'.$workingQueryString;
	}

	// ------------------------------------------------------------------------------------------------------------------------------
	// ------------------------------------------------------------------------------------------------------------------------------
	// $format can be 'url' or 'input'
	function outputQueryStringArgs($format = 'url', $columnToSortOn = null, $toggleSort = true, $filtersToExclude = array(), $validHtml = true) {
		$workingString = '';

		$queryStringArgs = $this->queryStringArgs;

		// ensure we output the correct version of ordercol
		foreach($queryStringArgs AS $arg => $val)
			if($arg == 'ordercol')
				$queryStringArgs[$arg] = array_search($val, $this->allowedOrderByCols);

		foreach($queryStringArgs AS $arg => $val)
			if(!in_array($arg, $filtersToExclude))
				if(gettype($val) == 'array')
					foreach($val AS $value)
						if($format == 'input')
							$workingString .= '<input type="hidden" name="'.htmlspecialchars($arg).'[]" value="'.htmlspecialchars($value).'" />';
							// id="'.htmlspecialchars(convertToUrl($arg.'_'.$value)).'"
						else
							$workingString .= '&amp;'.urlencode($arg).'[]='.urlencode($value);
				else
					if($format == 'input')
						$workingString .= '<input type="hidden" name="'.htmlspecialchars($arg).'" value="'.htmlspecialchars($val).'" />';
						// id="'.htmlspecialchars(convertToUrl($arg.'_'.$val)).'"
					else
						$workingString .= '&amp;'.urlencode($arg).'='.urlencode($val);

		return $workingString;
	}

	// ------------------------------------------------------------------------------------------------------------------------------
	// ------------------------------------------------------------------------------------------------------------------------------
	// generate an array (page number => href) (current page will have no href)
	function generatePaging($withNextPrevLinks = false) {
		$pageNav = array();
		if($this->paging == 'paged' AND $this->quantityPerPage > 0) {
			$numberOfResults = $this->numberOfResults;
			$resultsPerPage = $this->quantityPerPage;
			$totalPages = ceil($numberOfResults / $resultsPerPage);
			$currentPage = $this->currentPage ? ($this->currentPage <= $totalPages ? $this->currentPage : 1) : 1;
			$firstPageRow = ($currentPage - 1) * $resultsPerPage;

			$this->startRow = ($currentPage-1) * $resultsPerPage;
			if($currentPage * $resultsPerPage > $numberOfResults)
				$this->endRow = $numberOfResults;
			else
				$this->endRow = $currentPage * $resultsPerPage;

			// Create page nav string...
			if($totalPages > 1) {
				// Previous link...
				if($currentPage != 1 AND $withNextPrevLinks == true) {
					$pageNav[]['&lt; &lt;'] = $this->getSortLink(null, false).'&amp;page=1';
					$pageNav[]['&lt;'] = $this->getSortLink(null, false).'&amp;page='.($currentPage - 1);
				}

				$haveShownANumber = false;
				for($loop = 1; $loop <= $totalPages; $loop++) {
					if($totalPages < 8 // show all links
						OR ($totalPages >= 8 AND ($loop <= 3 OR $loop > ($totalPages - 1))) // show first 3 and last 1 links
						OR ($loop >= ($currentPage - 1) AND $loop <= ($currentPage + 1))) { // show immediately preceeding and following links
						if($loop == $currentPage)
							$pageNav[][$loop] = '';
						else
							$pageNav[][$loop] = $this->getSortLink(null, false).'&amp;page='.$loop;
						$haveShownANumber = true;
					}
					elseif($haveShownANumber) {
						$pageNav[]['...'] = '';
						$haveShownANumber = false;
					}
				}

				// Next link...
				if($currentPage + 1 <= $totalPages AND $withNextPrevLinks == true) {
					$pageNav[]['&gt;'] = $this->getSortLink(null, false).'&amp;page='.($currentPage + 1);
					$pageNav[]['&gt; &gt;'] = $this->getSortLink(null, false).'&amp;page='.$totalPages;
				}
				$pageNav[]['View all'] = $this->getSortLink(null, false).'&amp;paging=viewall';
			}
		}
		else {
			$pageNav[]['Paged results'] = $this->getSortLink(null, false).'&amp;paging=paged';
			$this->endRow = $this->numberOfResults;
		}

		return $pageNav;
	}
}

?>