<?php

if($_SERVER['SERVER_ADDR'] == '129.0.0.1') error_reporting(E_ALL);

if(get_magic_quotes_gpc()) {
	function stripslashes_deep($value) {
		$value = is_array($value) ?
					array_map('stripslashes_deep', $value) :
					stripslashes($value);

		return $value;
	}

	$_POST = array_map('stripslashes_deep', $_POST);
	$_GET = array_map('stripslashes_deep', $_GET);
	$_COOKIE = array_map('stripslashes_deep', $_COOKIE);
	$_REQUEST = array_map('stripslashes_deep', $_REQUEST);
}

if(!isset($customContentType)) $customContentType = 'text/html';
if((!isset($argv) OR count($argv) == 0) AND !isset($skipContentTypeHeader)) {
	session_start();
	header('Content-type: '.$customContentType.'; charset=utf-8'); // unless running on the command line or want to specifically suppress this header
}

/* START DATABASE */
require_once(BASE_PATH.'/includes/db_mysql.inc');

class some_db extends DB_Sql
{
	// have to use " here
	var $Host      = DB_HOST;
	var $Database  = DB_NAME;
	var $User      = DB_USER;
	var $Password  = DB_PASSWORD;
};
		
$db = new some_db;
$db->query('SET NAMES \'utf8\'');
/* END DATABASE */
define('_DOC_ROOT_', getenv('DOCUMENT_ROOT'));
define('_UPLOAD_DIR_HTTP_', '/uploaded-files');
define('_UPLOAD_DIR_', getenv('DOCUMENT_ROOT')._UPLOAD_DIR_HTTP_);
define('_WEBSITE_EMAIL_ADDRESS_', 'webmaster@'.$_SERVER['SERVER_NAME']);
define('_SQUARE_FEET_TO_SQUARE_METRES_', 0.09290304); // http://www.google.co.uk/#hl=en&xhr=t&q=100+sq+ft+in+sq+meters
define('_SQUARE_FEET_TO_ACRES_', 0.0000229568411); // http://www.google.co.uk/search?q=100+sq+ft+in+acres

$imageSizes = array('cms-small.jpg' => array('width' => 152, 'height' => 102),
					'cms-large.jpg' => array('width' => 608, 'height' => 408),
					'property-detail_t.jpg' => array('width' => 90, 'height' => 67),
					'property-detail.jpg' => array('width' => 524, 'height' => 349),
					'property-listing.jpg' => array('width' => 270, 'height' => 180),
					'property-map.jpg' => array('width' => 75, 'height' => 50),
					'property-featured.jpg' => array('width' => 214, 'height' => 214),
					'listing-large.jpg' => array('width' => 538, 'height' => 300));

$areaSizeTypes = array('gia' => 'GIA', 'nia' => 'NIA');
$areaSizeUnits = array('sqft' => 'Sq Ft', 'sqm' => 'Sq m', 'acres' => 'Acres');
$areaAvailabilities = array('forsale' => 'For Sale', 'tolet' => 'To Let', 'occupied' => 'Occupied', 'managed' => 'Managed', 'underoffer' => 'Under Offer', 'let' => 'Let', 'sold' => 'Sold');
$areaServiceChargePeriods = array('annum' => 'Per annum', 'sqft' => 'Per sq ft', 'sqm' => 'Per sq m', 'acre' => 'Per acre', 'month' => 'Per month');

$propertyPublishedStatusOptions = array('draft' => 'Not published', 'published' => 'Published to site', 'deleted' => 'Deleted'); // deleted shouldn't be shown but included here for completeness
$propertyFeaturedOptions = array('t' => 'Yes', 'f' => 'No');

$contactSourceOptions = array('web' => 'Web site', 'tel' => 'Telephone', 'email' => 'E-mail');

$sizeRangeTypes = array(	'0-500' => array(		'name' => '0 - 500 Sq ft',
													'count' => '0',
													'numericStart' => 0,
													'numericEnd' => 500),
							'500-1000' => array(	'name' => '500 - 1,000 Sq ft',
													'count' => '0',
													'numericStart' => 500,
													'numericEnd' => 1000),
							'1000-2000' => array(	'name' => '1,000 - 2,000 Sq ft',
													'count' => '0',
													'numericStart' => 1000,
													'numericEnd' => 2000),
							'2000-3000' => array(	'name' => '2,000 - 3,000 Sq ft',
													'count' => '0',
													'numericStart' => 2000,
													'numericEnd' => 3000),
							'3000-4000' => array(	'name' => '3,000 - 4,000 Sq ft',
													'count' => '0',
													'numericStart' => 3000,
													'numericEnd' => 4000),
							'4000-5000' => array(	'name' => '4,000 - 5,000 Sq ft',
													'count' => '0',
													'numericStart' => 4000,
													'numericEnd' => 5000),
							'5000-10000' => array(	'name' => '5,000 - 10,000 Sq ft',
													'count' => '0',
													'numericStart' => 5000,
													'numericEnd' => 10000),
							'10000-25000' => array(	'name' => '10,000 - 25,000 Sq ft',
													'count' => '0',
													'numericStart' => 10000,
													'numericEnd' => 25000),
							'25000plus' => array(	'name' => '25,000 Sq ft +',
													'count' => '0',
													'numericStart' => 25000,
													'numericEnd' => null));

function outputSelect($name, $values, $selected, $hasOptGroups = false, $onChangeJavaScript = false, $class = false, $disabled = false, $firstEntryText = 'Please select', $style = '', $multiple = false, $allowNameWithHtml = false) {

	if(!is_array($selected)) $selected = array($selected);

	$output = '<select name="'.($allowNameWithHtml === true ? $name : htmlspecialchars($name)).($multiple === true ? '[]' : '').'" id="'.htmlspecialchars($name).'"'.
					($onChangeJavaScript !== false ? ' onchange="JavaScript: if(this.selectedIndex != 0) '.$onChangeJavaScript.'"' : '').
					($class !== false ? ' class="'.$class.'"' : '').
					($disabled === true ? ' disabled="disabled"' : '').
					($style != '' ? ' style="'.$style.'"' : '').
					($multiple === true ? ' multiple="multiple"' : '').
				'>';
	if($firstEntryText !== null) $output .= '<option value="">'.htmlspecialchars($firstEntryText).'</option>';

	if($hasOptGroups) {
		$currentOptGroup = null;

		foreach($values AS $key => $val) {
			$output .= '<optgroup label="'.htmlspecialchars($key).'">';
			foreach($val AS $i => $value)
				$output .= '<option value="'.htmlspecialchars($value).'"'.(in_array($value, $selected) ? ' selected="selected"' : '').'>'.htmlentities($value).'</option>';
			$output .= '</optgroup>';
		}
	}
	else
		foreach($values AS $key => $val)
			$output .= '<option value="'.htmlspecialchars($key).'"'.(in_array($key, $selected) ? ' selected="selected"' : '').'>'.htmlspecialchars($val).'</option>';

	$output .= '</select>';

	return $output;
}

function escapeForDb($value, $canBeNull = false, $type = 'string') {
	if($canBeNull === true AND ($value === null OR $value === '')) return 'NULL';

	switch($type) {
		case 'int': { $value = intval($value); break; }
		case 'unixtime': { $value = 'FROM_UNIXTIME('.intval($value).')'; break; }
		case 'float': { $value = floatval($value); break; }
		case 'string':
		default: { $value = '\''.mysql_real_escape_string($value).'\''; break; }
	}
	return $value;
}

function createBreadCrumb($args, $includeLinks = true, $separator = ' &gt; ', $includeHome = true) {
	$items = array();
	if($includeHome) {
		if($includeLinks) $items[] = '<a href="/">Home</a>';
		else $items[] = 'Home';
	}

	$i = 1;
	$maxI = count($args);
	foreach($args as $url => $value) {
		$item = array();

		if($includeLinks AND $i < $maxI) $item[] = '<a href="'.$url.'">'; // can't htmlspecialchars here
		$item[] = htmlspecialchars($value);
		if($includeLinks AND $i < $maxI) $item[] = '</a>';

		$items[] = implode('', $item);
		$i++;
	}

	return implode($separator, $items);
}

function prepareForUrl($aString) {
	return urlencode(str_replace(' ', '-', str_replace('-', '---', strtolower($aString))));
}

function unprepareFromUrl($aString) {
	return str_replace('   ', '-', str_replace('-', ' ', stripslashes(urldecode($aString))));
}

function doRedirect($dest) {
	header('HTTP/1.1 301 Moved Permanently');
	header('Location: '.$dest);
	exit;
}

function getImageFilename($id, $i, $includeDir = true, $section = 'news') {
	global $imageSizes;

	$downloadDir = _UPLOAD_DIR_.'/'.$section.'/'.$id.'/images/'.$i;

	if(!is_dir($downloadDir)) return false;

	$imageFilenames = array_keys($imageSizes);

	$files = glob($downloadDir.'/*.*');

	foreach($files AS $entry)
		if(!in_array(str_replace($downloadDir.'/', '', $entry), $imageFilenames))
			return $includeDir ? $entry : str_replace($downloadDir.'/', '', $entry);

	return false;
}

function getDownloadFilename($id, $i, $includeDir = true, $section, $fileType = 'file') {
	$downloadDir = _UPLOAD_DIR_.'/'.$section.'/'.$id.'/'.$fileType.'s/'.$i;

	if(!is_dir($downloadDir)) return false;

	$files = glob($downloadDir.'/*.*');
	if(count($files) == 1) {
		$fileName = array_pop($files);
		return $includeDir ? $fileName : str_replace($downloadDir.'/', '', $fileName);
	}
	else return false;
}

function getMimeType($fileName) {
	$type = '';
	$extension = str_replace('.', '', strtolower(strrchr($fileName, '.')));
	switch($extension) {
		case 'pdf': { $type = 'pdf'; break; }
		case 'doc': case 'docx': { $type = 'doc'; break; }
		case 'ppt': case 'pptx': { $type = 'ppt'; break; }
		case 'xls': case 'xlsx': { $type = 'xls'; break; }
	}
	return $type;
}

// returns a string no longer than "maxLength". The string may be shortened up to the last space
// e.g. "The quick brown fox", with $maxLength=18 will result in "The quick brown"
// $trailingCharacters won't be added unless specified and will only be added if the string has been truncated (they would typically be "...")
// ignores any new line or tab characters
function getSubstringWithCleanEnding($aString, $maxLength, $trailingCharacters = '') {
	if(strlen($aString) <= $maxLength) return $aString;

	$workingString = substr($aString, 0, $maxLength); // truncate the string

	if($aString{$maxLength} == ' ') return $workingString.$trailingCharacters; // if the next character is a space then we can return the current string

	return substr($workingString, 0, strripos($workingString, ' ')).$trailingCharacters; // return the string up to the last space
}

function processArgsFromUrl() {
	// http://blog.pumka.net/2009/12/30/rewriting-for-seo-friendly-urls-htaccess-or-php/

	list($path) = explode('?', $_SERVER['REQUEST_URI']); // remove request parameters

	$path = substr($path, strlen(dirname($_SERVER['SCRIPT_NAME']))+1); // remove script path

	// explode path to directories and remove empty items
	$pathInfo = array();
	foreach(explode('/', $path) as $dir) if(!empty($dir)) $pathInfo[] = urldecode($dir);

	if(count($pathInfo) > 0) {
		//Remove file extension from the last element:
		$last = $pathInfo[count($pathInfo)-1];
		list($last) = explode('.', $last);
		$pathInfo[count($pathInfo)-1] = $last;
	}

	return $pathInfo;
}

function formatBytes($size) {
	$units = array(' B', ' KB', ' MB', ' GB', ' TB');
	for ($i = 0; $size >= 1024 && $i < 4; $i++) $size /= 1024;
	return round($size, 2).$units[$i];
}

function createOffSiteLink($url, $name = null, $onclickJavaScript = false) {
	$output = '';
	if($url != '') {
		$output = '<a href="';
		if(stripos($url, 'http://') === false) $output .= 'http://';
		$output .= htmlspecialchars($url);
		$output .= '" onclick="'.($onclickJavaScript !== false ? $onclickJavaScript.'; ' : '').'return !window.open(this.href);">'.htmlspecialchars($name != null ? $name : $url).'</a>';
	}

	return $output;
}

// $validSeparator: true = &amp;, false = &
function outputArgs($argsToIgnore, $validSeparator = true) {
	global $filters;

	$outputArgs = array();

	foreach($filters as $arg => $val)
		if(!in_array($arg, $argsToIgnore))
			$outputArgs[] = urlencode($arg).'='.urlencode($val);

	return implode($validSeparator ? '&amp;' : '&', $outputArgs);
}

// $address must be ready to go straight to Google Maps
// outputs the coords in an array: (lon, lat)
function getCoordinates($address) {
	global $db;

	$sql = 'SELECT * FROM coords_cache WHERE address = \''.mysql_real_escape_string($address).'\' AND meta_inserted > DATE_SUB(NOW(), INTERVAL 7 DAY)';
	$db->query($sql);
	if($db->num_rows() > 0) {
		$db->next_record();
		return array($db->f('lon'), $db->f('lat'));
	}

	$google_api_key = _GOOGLE_MAPS_API_;
	$google_base_url = "http://maps.google.co.uk/maps/geo?output=xml&key=" . $google_api_key;
	$request_url = $google_base_url . "&q=" . urlencode($address);
	$geocode_pending = true;
	$delay = 0;

	while ($geocode_pending) {
		$xml = simplexml_load_file($request_url) or die("url not loading");
		$status = $xml->Response->Status->code;

		if (strcmp($status, "200") == 0) { // success
			$geocode_pending = false;
			$coordinates = $xml->Response->Placemark->Point->coordinates;
			$coords = explode(",", $coordinates);

			$sql = 'INSERT INTO coords_cache
					SET address = \''.mysql_real_escape_string($address).'\',
						lat = '.floatval($coords[1]).',
						lon = '.floatval($coords[0]).',
						meta_inserted = NOW()';
			$db->query($sql);

			return explode(",", $coordinates);
		} else if (strcmp($status, "620") == 0) { // too fast
			$delay += 5;
		} else { // failed
			$geocode_pending = false;
		}
		sleep($delay);
	}
	return false;
}

function formatPropertyPrice($thePrice) {
	if($thePrice == 0)
		return 'n/a';
	elseif(round($thePrice) > 1000000)
		return round(round($thePrice) / 1000000, 1).'m';
	else
		return number_format(round($thePrice));
}

// units come from Caldes in sq m
// http://www.ilpi.com/msds/ref/areaunits.html: "One sq m = 10.764 sq feet"
function formatPropertySize($minSize, $maxSize) {
	if($minSize == $maxSize)
		return number_format(round($minSize));
	else
		return number_format(round($minSize)).' to '.number_format(round($maxSize));
}

// takes a number and rounds it to the nearest base 10
// (949, 'up') will be 1000
// (949, 'down') will 900
// (94, 'up') will be 100
// (94, 'down') will 90
function roundToNearestBaseTen($number, $roundUpOrDown = 'up') {
	$numberOfDigits = strlen($number);
	if($roundUpOrDown == 'up')
		return ceil($number / pow(10, $numberOfDigits - 1)) * pow(10, $numberOfDigits - 1);
	else
		return floor($number / pow(10, $numberOfDigits - 1)) * pow(10, $numberOfDigits - 1);
}

function getPropertysTypes($id) {
	global $db;

	$types = array();

	$sql = 'SELECT * FROM property_building_types, building_types WHERE property_id = '.intval($id).' AND building_type_id = meta_id AND meta_status = \'published\' ORDER BY name ASC';
	$db->query($sql);
	while($db->next_record())
		$types[$db->f('meta_id')] = $db->f('name');

	return $types;
}

/*function getPropertysAvailabilities($id) {
	global $db;

	$availabilities = array();

	$sql = 'SELECT * FROM property_availability_types, availability_types WHERE property_id = '.intval($id).' AND availability_type_id = meta_id AND meta_status = \'published\' ORDER BY name ASC';
	$db->query($sql);
	while($db->next_record())
		$availabilities[$db->f('meta_id')] = $db->f('name');

	return $availabilities;
}*/

function getPropertysAssignedUsers($id) {
	global $db;

	$users = array();

	$sql = 'SELECT * FROM property_assigned_users, users WHERE property_id = '.intval($id).' AND user_id = meta_id AND meta_status != \'deleted\' ORDER BY surname ASC, forename ASC';
	$db->query($sql);
	while($db->next_record())
		$users[$db->f('meta_id')] = $db->f('forename').' '.$db->f('surname');

	return $users;
}

function getContactsAssignedUsers($id) {
	global $db;

	$users = array();

	$sql = 'SELECT * FROM contact_assigned_users, users WHERE contact_id = '.intval($id).' AND user_id = meta_id AND meta_status != \'deleted\' ORDER BY surname ASC, forename ASC';
	$db->query($sql);
	while($db->next_record())
		$users[$db->f('meta_id')] = $db->f('forename').' '.$db->f('surname');

	return $users;
}

function getPropertyBuildingTypes() {
	global $db;

	$types = array();

	$sql = 'SELECT * FROM building_types WHERE meta_status = \'published\' ORDER BY meta_order ASC, name ASC';
	$db->query($sql);
	while($db->next_record())
		$types[$db->f('meta_id')] = $db->f('name');

	return $types;
}

// not to be used for log in related logic
function getUsers() {
	global $db;

	$types = array();

	$sql = 'SELECT *, users.meta_id AS user_meta_id, offices.title AS office_title
			FROM users LEFT JOIN offices ON (users.office = offices.meta_id AND offices.meta_status = \'published\')
			WHERE users.meta_status IN (\'live\', \'disabled\')
			ORDER BY surname ASC, forename ASC';
	$db->query($sql);
	while($db->next_record())
		$types[$db->f('user_meta_id')] = $db->f('forename').' '.$db->f('surname').' ('.($db->f('company') != '' ? $db->f('company') : 'No company').')';

	return $types;
}

function getAvailabilityStatuses() {
	global $db;

	$types = array();

	$sql = 'SELECT * FROM availability_statuses WHERE meta_status = \'published\'';
	$db->query($sql);
	while($db->next_record())
		$types[$db->f('meta_id')] = $db->f('name');

	return $types;
}

function getPersonTitles() {
	global $db;

	$titles = array();

	$sql = 'SELECT * FROM person_titles ORDER BY meta_order ASC';
	$db->query($sql);
	while($db->next_record())
		$titles[$db->f('meta_id')] = $db->f('name');

	return $titles;
}

function processAllSpecificationAreaRows() {
	$rowData = array();

	$i = 0;
	foreach($_REQUEST['rowtype'] AS $buildingType => $groups) {
		foreach($groups AS $key => $val) {
			if($val == 'group')
				$currentGroup = $i; // $_REQUEST['areaname'][$key];
			else
				if(isset($rowData[$currentGroup])) $rowData[$currentGroup]++; else $rowData[$currentGroup] = 0; // = $_REQUEST['areaname'][$key];
			$i++;
		}
	}
	return $rowData;
}

function loadSpecifications($includeList = array(), $excludeList = array()) {
	global $db;

	$specifications = array();

	$sql = 'SELECT * FROM specifications ';

	$whereClauses = array('meta_status = \'published\'');
	if(count($includeList) > 0) $whereClauses[] = ' meta_id IN ('.implode(', ', $includeList).')';
	if(count($excludeList) > 0) $whereClauses[] = ' meta_id NOT IN ('.implode(', ', $excludeList).')';
	if(count($whereClauses) > 0) $sql .= ' WHERE '.implode(' AND ', $whereClauses).' ';

	$orderByClauses = array();
	if(count($includeList) > 0) $orderByClauses[] = ' FIELD(meta_id, '.implode(', ', $includeList).')';
	if(count($excludeList) > 0) $orderByClauses[] = ' FIELD(meta_id, '.implode(', ', $excludeList).')';
	$orderByClauses[] = 'name ASC';
	if(count($orderByClauses) > 0) $sql .= ' ORDER BY '.implode(', ', $orderByClauses).' ';

	$db->query($sql);
	while($db->next_record())
		$specifications[$db->f('meta_id')] = $db->f('name');

	return $specifications;
}

function getFirstParagraphEndPoint($text) {
	$text = str_replace("\r", "\n", $text);
	return stripos($text, "\n");
}

// convert UK date into a Unix timestamp
function ukDateToTimestamp($someDate) {
	$theDateParts = array();
	$theDateParts = explode('/', $someDate);

	//if(count($theDateParts) == 3) $someDate = mktime(0, 0, 0, intval($theDateParts[1]), intval($theDateParts[0]), intval($theDateParts[2]));
	//else $someDate = 0;
	$someDate = @strtotime($someDate);
	return $someDate;
}

// convert US date into a Unix timestamp
function usDateToTimestamp($someDate) {
	$theDateParts = array();
	$theDateParts = explode('/', $someDate);

	if(count($theDateParts) == 3) $someDate = mktime(0, 0, 0, intval($theDateParts[0]), intval($theDateParts[1]), intval($theDateParts[2]));
	else $someDate = 0;

	return $someDate;
}

function mysqlToUsDate($someDate) {
	return substr($someDate, 5, 2).'/'.substr($someDate, 8, 2).'/'.substr($someDate, 0, 4);
}

function mysqlToUkDate($someDate) {
	return substr($someDate, 8, 2).'/'.substr($someDate, 5, 2).'/'.substr($someDate, 0, 4);
}

function timestampToMysqlDate($someDate) {
	return date('Y-m-d', $someDate);
}

// get properties' details that are stored in other tables
function getAssignmentQuantities($propertyIds, $sourceTable, $linkTable, $linkField, $fields = 'name') { // quick function to load in a table
	global $db;

	if(count($propertyIds) == 0) return array();

	$quantities = $propertiesProcessed = array();
	if(!is_array($fields)) $fields = array($fields);

	$sql = 'SELECT *
			FROM '.mysql_real_escape_string($linkTable).', '.mysql_real_escape_string($sourceTable).'
			WHERE property_id IN ('.implode(', ', $propertyIds).')
				AND '.mysql_real_escape_string($linkField).' = meta_id
				AND '.($sourceTable == 'users' ? 'meta_status != \'deleted\'' : 'meta_status = \'published\'');
	if($sourceTable == 'building_types') $sql .= ' ORDER BY meta_order ASC ';
	$db->query($sql);
	while($db->next_record()) {
		if(!in_array($db->f('property_id'), $propertiesProcessed)) $propertiesProcessed[] = $db->f('property_id');

		if(isset($quantities[$db->f('meta_id')]))
			$quantities[$db->f('meta_id')]['count']++;
		else {
			$names = array();
			foreach($fields AS $field) $names[] = $db->f($field);
			$quantities[$db->f('meta_id')] = array('name' => implode(', ', $names), 'count' => 1);
		}
	}

	// add in any that haven't been assigned values
	$unassignedProperties = count($propertyIds) - count($propertiesProcessed);
	if($unassignedProperties > 0) $quantities[0] = array('name' => 'Unassigned', 'count' => $unassignedProperties);

	// now sort the results
	if($sourceTable != 'building_types') uasort($quantities, 'compareAssignmentQuantitiesCounts');

	return $quantities;
}

// get properties' details that are stored in other tables
function getAssignmentQuantitiesContacts($propertyIds, $sourceTable, $linkTable, $linkField, $fields = 'name') { // quick function to load in a table
	global $db;

	if(count($propertyIds) == 0) return array();

	$quantities = $propertiesProcessed = array();
	if(!is_array($fields)) $fields = array($fields);

	$sql = 'SELECT *
			FROM '.mysql_real_escape_string($linkTable).', '.mysql_real_escape_string($sourceTable).'
			WHERE contact_id IN ('.implode(', ', $propertyIds).')
				AND '.mysql_real_escape_string($linkField).' = meta_id
				AND '.($sourceTable == 'users' ? 'meta_status != \'deleted\'' : 'meta_status = \'published\'');
	if($sourceTable == 'building_types') $sql .= ' ORDER BY meta_order ASC ';
	$db->query($sql);
	while($db->next_record()) {
		if(!in_array($db->f('contact_id'), $propertiesProcessed)) $propertiesProcessed[] = $db->f('contact_id');

		if(isset($quantities[$db->f('meta_id')]))
			$quantities[$db->f('meta_id')]['count']++;
		else {
			$names = array();
			foreach($fields AS $field) $names[] = $db->f($field);
			$quantities[$db->f('meta_id')] = array('name' => implode(', ', $names), 'count' => 1);
		}
	}

	// add in any that haven't been assigned values
	$unassignedProperties = count($propertyIds) - count($propertiesProcessed);
	if($unassignedProperties > 0) $quantities[0] = array('name' => 'Unassigned', 'count' => $unassignedProperties);

	// now sort the results
	if($sourceTable != 'building_types') uasort($quantities, 'compareAssignmentQuantitiesCounts');

	return $quantities;
}

function compareAssignmentQuantitiesCounts($a, $b) {
	if($a['name'] == $b['name']) return 0;

	return ($a['name'] < $b['name']) ? -1 : 1;

	// the two lines below will sort by "count", then "name"
	if($a['count'] == $b['count']) return ($a['name'] < $b['name']) ? -1 : 1;

	return ($a['count'] > $b['count']) ? -1 : 1;
}

// load in the tenure types
function loadTenureTypes($propertyIds, $forFrontEnd = true) {
	global $db, $areaAvailabilities;

	if(count($propertyIds) == 0) return array();

	$quantities = $propertiesProcessed = array();

	$sql = 'SELECT *
			FROM property_areas
			WHERE property_id IN ('.implode(', ', $propertyIds).')
			GROUP BY CONCAT(property_id, areaavailability)';
	$db->query($sql);
	while($db->next_record()) {
		if(!isset($areaAvailabilities[$db->f('areaavailability')])) continue; // the availability has probably been left empty for this area so we'll skip it

		if($forFrontEnd AND $db->f('areaavailability') == 'let') continue;

		if(!in_array($db->f('property_id'), $propertiesProcessed)) $propertiesProcessed[] = $db->f('property_id');

		if(isset($quantities[$db->f('areaavailability')]))
			$quantities[$db->f('areaavailability')]['count']++;
		else
			$quantities[$db->f('areaavailability')] = array('name' => $areaAvailabilities[$db->f('areaavailability')], 'count' => 1);
	}

	// add in any that haven't been assigned values
	if(!$forFrontEnd) {
		$unassignedProperties = count($propertyIds) - count($propertiesProcessed);
		if($unassignedProperties > 0) $quantities[0] = array('name' => 'Unassigned', 'count' => $unassignedProperties);
	}

	// now sort the results
	uasort($quantities, 'compareAssignmentQuantitiesCounts');

	return $quantities;
}

function updateLowestPrice($currentLowestPrice, $areaavailability_publish, $arearent_publish, $arearent) {
	if($areaavailability_publish == 't' AND $arearent_publish == 't' AND $arearent != '') {
		// first time through
		if($currentLowestPrice == null)
			$currentLowestPrice = floatval($arearent);

		if(floatval($arearent) < $currentLowestPrice)
			$currentLowestPrice = floatval($arearent);
	}
	return $currentLowestPrice;
}

// this function populates the four passed variables as well as the global $sizeRangeTypes
// $sizeRangeTypes is tricky because we need to make sure that the "count" value is only incremented once for any matching property area size
// $propertyAreaSizeRanges is an array containing each property's area's minimum and maximum sizes
function getSizePriceRanges($propertyIds, &$minFloorArea, &$maxFloorArea, &$minPrice, &$maxPrice, &$propertyAreaSizeRanges) {
	global $db, $areaAvailabilities, $sizeRangeTypes;

	if(count($propertyIds) == 0) return array();

	$quantities = $propertiesProcessed = array();

	$rangeProperties = array(); // this store the number of unique properties with a size in each range

	$sql = 'SELECT *
			FROM property_areas
			WHERE property_id IN ('.implode(', ', $propertyIds).')'; // AND area_include_in_totals = \'t\'';
	$db->query($sql);
	while($db->next_record()) {
		$lowestPriceRental = $lowestPriceRentalPeriod = $lowestPriceForSale = null;
		if($db->f('areasize_publish') == 't' and $db->f('area_include_in_totals') == 't' ) {
			if(intval($db->f('areasize_feet')) < $minFloorArea OR $minFloorArea === null) $minFloorArea = intval($db->f('areasize_feet'));
			if(intval($db->f('areasize_feet')) > $maxFloorArea OR $maxFloorArea === null) $maxFloorArea = intval($db->f('areasize_feet'));

			foreach($sizeRangeTypes AS $key => $rangeDetails)
				if(
						(intval($db->f('areasize_feet')) >= $rangeDetails['numericStart'] AND ($rangeDetails['numericEnd'] == null OR intval($db->f('areasize_feet')) < $rangeDetails['numericEnd']))
					AND 
						(!isset($rangeProperties[$key]) OR !in_array(intval($db->f('property_id')), $rangeProperties[$key]))
					)
					$rangeProperties[$key][] = intval($db->f('property_id'));

			// see how this area's dimensions compare with those we already have for the property associated with this area
			if(!isset($propertyAreaSizeRanges[$db->f('property_id')]['minsize']) OR $propertyAreaSizeRanges[$db->f('property_id')]['minsize'] > intval($db->f('areasize_feet')))
				$propertyAreaSizeRanges[$db->f('property_id')]['minsize'] = intval($db->f('areasize_feet'));
			if(!isset($propertyAreaSizeRanges[$db->f('property_id')]['maxsize']) OR $propertyAreaSizeRanges[$db->f('property_id')]['maxsize'] < intval($db->f('areasize_feet')))
				$propertyAreaSizeRanges[$db->f('property_id')]['maxsize'] = intval($db->f('areasize_feet'));
		}

		if($db->f('arearate_publish') == 't' ) {
			if(intval($db->f('arearate')) < $minPrice OR $minPrice === null) $minPrice = intval($db->f('arearate'));
			if(intval($db->f('arearate')) > $maxPrice OR $maxPrice === null) $maxPrice = intval($db->f('arearate'));
		}

		// remove the indenting to try to fit as much on one line as possible (for readability)
		/*if(!isset($propertyAreaSizeRanges[$db->f('property_id')]['lowestPriceRental'])) $propertyAreaSizeRanges[$db->f('property_id')]['lowestPriceRental'] = null;
		if(!isset($propertyAreaSizeRanges[$db->f('property_id')]['lowestPriceForSale'])) $propertyAreaSizeRanges[$db->f('property_id')]['lowestPriceForSale'] = null;
		
		if($db->f('areaavailability_publish') == 't' AND $db->f('arearent_publish') == 't' AND $db->f('arearent') != '') {
			if($db->f('areaavailability') == 'tolet' AND $propertyAreaSizeRanges[$db->f('property_id')]['lowestPriceRental'] == null)
				$propertyAreaSizeRanges[$db->f('property_id')]['lowestPriceRental'] = intval($db->f('arearent'));
			elseif($db->f('areaavailability') == 'forsale' AND $propertyAreaSizeRanges[$db->f('property_id')]['lowestPriceForSale'] == null)
				$propertyAreaSizeRanges[$db->f('property_id')]['lowestPriceForSale'] = intval($db->f('arearent'));
		
			if($db->f('areaavailability') == 'tolet' AND intval($db->f('arearent')) < $propertyAreaSizeRanges[$db->f('property_id')]['lowestPriceRental'])
				$propertyAreaSizeRanges[$db->f('property_id')]['lowestPriceRental'] = intval($db->f('arearent'));
			elseif($db->f('areaavailability') == 'forsale' AND intval($db->f('arearent')) < $propertyAreaSizeRanges[$db->f('property_id')]['lowestPriceForSale'])
				$propertyAreaSizeRanges[$db->f('property_id')]['lowestPriceForSale'] = intval($db->f('arearent'));
		}*/
		
		if(isset($propertyAreaSizeRanges[$db->f('property_id')]['lowestPriceRental'])) {
			$lowestPriceRental = $propertyAreaSizeRanges[$db->f('property_id')]['lowestPriceRental'];
		}
		if(isset($propertyAreaSizeRanges[$db->f('property_id')]['lowestPriceForSale'])) $lowestPriceForSale = $propertyAreaSizeRanges[$db->f('property_id')]['lowestPriceForSale'];
		
		#echo '|'.$db->f('property_id').'-'.$db->f('building_type_id').'.'.$db->f('area_group_id').$db->f('areaavailability');
		if($db->f('areaavailability') == 'tolet') {
			$lowestPriceRental = updateLowestPrice($lowestPriceRental, $db->f('areaavailability_publish'), $db->f('arearent_publish'), $db->f('arearent'));
			$lowestPriceRentalPeriod = $db->f('areaservicechargeperiod');
		}
		if($db->f('areaavailability') == 'forsale')
			$lowestPriceForSale = updateLowestPrice($lowestPriceForSale, $db->f('areaavailability_publish'), $db->f('arearent_publish'), $db->f('arearent'));
		
		$propertyAreaSizeRanges[$db->f('property_id')]['lowestPriceRental'] = $lowestPriceRental;
		$propertyAreaSizeRanges[$db->f('property_id')]['lowestPriceRentalPeriod'] = $lowestPriceRentalPeriod;
		$propertyAreaSizeRanges[$db->f('property_id')]['lowestPriceForSale'] = $lowestPriceForSale;

	}

	foreach($sizeRangeTypes AS $key => $rangeDetails)
		if(isset($rangeProperties[$key]))
			$sizeRangeTypes[$key]['count'] = count($rangeProperties[$key]);

	return true;
}

// quick simple function to process the results from a property table look-up and return the main image URL for the property or the default image
function getPropertyImage($propertyId, $imageId, $filename, $default) {
	if(intval($imageId) != 0) {
		$filePath = '/property/'.intval($propertyId).'/images/'.intval($imageId);
		if(is_file(_UPLOAD_DIR_.$filePath.'/'.$filename))
			return _UPLOAD_DIR_HTTP_.$filePath.'/'.$filename;
	}
	return $default;
}

function outputPropertySizeDetails($smallestSize, $largestSize, $metric) {
	if($smallestSize == $largestSize) {
		if($smallestSize == 0)
			$sizeRangeFeet = $sizeRangeMetres = '-';
		#$sizeRangeFeet = number_format($smallestSize);
		#$sizeRangeMetres = number_format($smallestSize * _SQUARE_FEET_TO_SQUARE_METRES_, 2);
		if($metric == 'metres')
			return number_format($smallestSize * _SQUARE_FEET_TO_SQUARE_METRES_, 2);
		else
			return number_format($smallestSize);
	}
	else {
		if($metric == 'metres')
			return number_format($smallestSize * _SQUARE_FEET_TO_SQUARE_METRES_, 2).' - '.number_format($largestSize * _SQUARE_FEET_TO_SQUARE_METRES_, 2);
		else
			return number_format($smallestSize).' - '.number_format($largestSize);
		#$sizeRangeFeet = number_format($smallestSize).' - '.number_format($largestSize);
		#$sizeRangeMetres = 
	}
}

function getAgentContactDetails($agentId) {
	global $db;

	$agentsDetails = array(	0 => array(		'forename' => 'Wadham and Isherwood',
											'surname' => '',
											'tel1' => '+44 (0)1483 300176',
											'email' => 'wandi.contact@tlgd.co.uk'),
							1 => array(		'forename' => 'Mark',
											'surname' => 'Parsonage',
											'tel1' => '+44 (0)1483 123456',
											'email' => 'mark@markparsonage.com'),
							2 => array(		'forename' => 'Michael',
											'surname' => 'Avery',
											'tel1' => '+44 (0)1483 123456',
											'email' => 'michael@tlgd.co.uk'),
							3 => array(		'forename' => 'Nick',
											'surname' => 'Reeve',
											'tel1' => '+44 (0)1252 710822',
											'email' => 'nick@wandi.co.uk'),
							4 => array(		'forename' => 'Peter',
											'surname' => 'Da Silva',
											'tel1' => '+44 (0)1483 300176',
											'email' => 'pds@wadhamandisherwood.co.uk'),
							5 => array(		'forename' => 'Nigel',
											'surname' => 'Wadham',
											'tel1' => '+44 (0)1483 300176',
											'email' => 'njw@wadhamandisherwood.co.uk'),
							6 => array(		'forename' => 'Mark',
											'surname' => 'Isherwood',
											'tel1' => '+44 (0)1483 300176',
											'email' => 'mjji@wadhamandisherwood.co.uk'),
							7 => array(		'forename' => 'Geoff',
											'surname' => 'Reeve',
											'tel1' => '+44 (0)1252 710822',
											'email' => 'geoff@wandi.co.uk'),
							11 => array(	'forename' => 'Chris',
											'surname' => 'Lock',
											'tel1' => '+44 (0)1252 710822',
											'email' => 'chris@wandi.co.uk'),
							12 => array(	'forename' => 'Peter',
											'surname' => 'Atkinson',
											'tel1' => '+44 (0)1483 300176',
											'email' => 'pja@wadhamandisherwood.co.uk'),
							13 => array(	'forename' => 'David',
											'surname' => 'Fitzpatrick',
											'tel1' => '+44 (0)1483 300176',
											'email' => 'dwjf@wadhamandisherwood.co.uk'),
							14 => array(	'forename' => 'Ruth',
											'surname' => 'Iles',
											'email' => 'wandi.contact@tlgd.co.uk',
											'tel1' => '+44 (0)1483 300176'),
							15 => array(	'forename' => 'Peter',
											'surname' => 'Miller',
											'tel1' => '+44 (0)1483 300176',
											'email' => 'wandi.contact@tlgd.co.uk'),
							16 => array(	'forename' => 'Graham',
											'surname' => 'Tring',
											'tel1' => '+44 (0)1483 300176',
											'email' => 'gta@wadhamandisherwood.co.uk'),
							17 => array(	'forename' => 'Tracey',
											'surname' => 'Wells',
											'tel1' => '+44 (0)1483 300176',
											'email' => 'trw@wadhamandisherwood.co.uk'),
							18 => array(	'forename' => 'Nikki',
											'surname' => 'James',
											'tel1' => '+44 (0)1483 300176',
											'email' => 'nej@wadhamandisherwood.co.uk'),
							19 => array(	'forename' => 'Steve',
											'surname' => 'Thwaites',
											'tel1' => '+44 (0)1252 710822',
											'email' => 'wif@wadhamandisherwood.co.uk'),
							20 => array(	'forename' => 'Steve',
											'surname' => 'Thwaites',
											'tel1' => '+44 (0)1252 710822',
											'email' => 'wif@wadhamandisherwood.co.uk'),				
							21 => array(	'forename' => 'Alan',
											'surname' => 'Browne',
											'tel1' => '+44 (0)1252 710822',
											'email' => 'abrowne@wadhamandisherwood.co.uk'));

	/*$sql = 'SELECT *
			FROM users LEFT JOIN offices ON (office = offices.meta_id
											AND offices.meta_status = \'published\')
			WHERE users.meta_status = \'live\' 
				AND users.meta_id = '.escapeForDb($agentId, false, 'int');
	$db->query($sql);
	if($db->num_rows() == 1) {
		$db->next_record();
		return $db->Record;
	}
	return false;*/

	return isset($agentsDetails[$agentId]) ? $agentsDetails[$agentId] : $agentsDetails[0];
}

function getRentToShow($areaavailability_publish, $arearent_publish, $arearent) {
	if($areaavailability_publish == 'f' OR $arearent_publish == 'f' OR $arearent == '')
		return '-';
	elseif($arearent == '0')
		return '0';
	else
		return number_format($arearent, 2);
}

?>